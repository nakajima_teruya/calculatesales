package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class CalculateSales {


	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";



	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_EXIT_ERROR = "が存在しません";
	private static final String SORT_ERROR = "売上ファイルが連番になっていません";
	private static final String SALE_FORMAT_ERROR = "のフォーマットが不正です";
	private static final String BRANCH_CODE_ERROR = "の支店コードが不正です";
	private static final String COMMODITY_CODE_ERROR = "の商品コードが不正です";
	private static final String TOTAL_ERROR = "合計金額が10桁を超えました";


	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<String, String>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<String, Long>();
		// 商品コードと商品名を保持するMap
		Map<String,String> commodityNames = new HashMap<String,String>();
		//　商品コードと売上金額を保持するMap
		Map<String,Long> commoditySales = new HashMap<String,Long>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "^[0-9]{3}$", "支店定義ファイル")) {
			return;
		}
		//商品定義ファイル読込処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "^[0-9a-zA-Z]{8}$", "商品定義ファイル")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		BufferedReader br = null;

		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(int i  = 0; i < files.length; i++) {

			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {

				rcdFiles.add(files[i]);

			}
		}

		Collections.sort(rcdFiles);

		for(int i = 0; i < rcdFiles.size() - 1; i++) {

			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if((latter - former) != 1) {
				System.out.println(SORT_ERROR);
				return;
			}
		}

		for(int i = 0; i < rcdFiles.size(); i++) {

			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				String fileEarnings;
				ArrayList<String> fileEarningsDetail = new ArrayList<>();

				while((fileEarnings = br.readLine()) != null) {

					fileEarningsDetail.add(fileEarnings);
				}

				if(fileEarningsDetail.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + SALE_FORMAT_ERROR);
					return;
				}

				if(!branchSales.containsKey(fileEarningsDetail.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + BRANCH_CODE_ERROR);
					return;
				}
				if(!commoditySales.containsKey(fileEarningsDetail.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + COMMODITY_CODE_ERROR);
					return;
				}


				if(!fileEarningsDetail.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				long fileSale = Long.parseLong(fileEarningsDetail.get(2));


				Long saleAmount = branchSales.get(fileEarningsDetail.get(0)) + fileSale;
				Long commoditySaleAmount = commoditySales.get(fileEarningsDetail.get(1)) + fileSale;


				if(saleAmount >= 10000000000L || commoditySaleAmount >= 10000000000L) {
					System.out.println(TOTAL_ERROR);
					return;
				}

				branchSales.put(fileEarningsDetail.get(0), saleAmount);
				commoditySales.put(fileEarningsDetail.get(1), commoditySaleAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;

			} finally {

				if(br != null) {
					try {

						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}



		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> codeNames, Map<String, Long> codeSales, String matchesPath, String definition) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			//読み込んだファイルが存在するか判断
			if(!file.exists()) {
				System.out.println(definition + FILE_EXIT_ERROR);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;

			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");

				if((items.length != 2) || (!items[0].matches(matchesPath))) {
					System.out.println(definition + SALE_FORMAT_ERROR);
					return false;
				}
				//支店コード, 支店名（商品コード、商品名）
				codeNames.put(items[0], items[1]);
				//支店コード, 売上金額（商品コード、売上金額）
				codeSales.put(items[0], 0L);


			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> codeNames, Map<String, Long> codeSales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		//ファイルへの書き込み処理を記載
		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//マップからキーを取得
			for (String key : codeNames.keySet()) {

				bw.write(key + "," + codeNames.get(key) + "," + codeSales.get(key));
				bw.newLine();

			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
